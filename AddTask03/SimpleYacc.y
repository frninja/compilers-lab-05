%{
// ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public Parser(AbstractScanner<int, LexLocation> scanner) : base(scanner) { }
%}

%output = SimpleYacc.cs

%namespace SimpleParser

%token BEGIN END CYCLE INUM RNUM ID ASSIGN SEMICOLON  PLUS MINUS MULT DIV LPAREN RPAREN

%%

progr   : block
		;

stlist	: statement 
		| stlist SEMICOLON statement 
		;

statement: assign
		| block  
		| cycle  
		;

ident 	: ID 
		;
	
assign 	: ident ASSIGN expr 
		;

expr	: T
		| expr PLUS T
		| expr MINUS T
		;

T		: F
		| T MULT F
		| T DIV F
		;

F		: ident  
		| INUM 
		| LPAREN expr RPAREN
		;

block	: BEGIN stlist END 
		;

cycle	: CYCLE expr statement 
		;
	
%%
