%{
// ��� ���������� ����������� � ����� GPPGParser, �������������� ����� ������, ������������ �������� gppg
    public Parser(AbstractScanner<int, LexLocation> scanner) : base(scanner) { }
%}

%output = SimpleYacc.cs

%namespace SimpleParser

%token BEGIN END CYCLE INUM RNUM ID ASSIGN SEMICOLON VAR COLON

%%

progr   : block
		;

stlist	: statement 
		| stlist SEMICOLON statement 
		;

var		: VAR decls
		;

decls	: decl
		| decls COLON decl
		;

decl	: ident
		;


statement: assign
		| block  
		| cycle  
		| var
		;

ident 	: ID 
		;
	
assign 	: ident ASSIGN expr 
		;

expr	: ident  
		| INUM 
		;

block	: BEGIN stlist END 
		;

cycle	: CYCLE expr statement 
		;
	
%%
